FROM golang:alpine

RUN apk add --no-cache git openssh
COPY . /app
WORKDIR /app
RUN cd /app
RUN ls -l
RUN go get github.com/franela/goblin
RUN go get -d -v ./...
RUN CGO_ENABLE=0 go build main.go
ENV GIN_MODE=release

CMD ./main $PORT
